package org.example.service;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

// TODO:
//  1. В тестах public не пишем
//  2. void - метод ничего не возвращает
//  3. @Test над методом - аннотация:
//    Это всё JUnit5 сделает за нас:
//    DistancePredictionServiceTest test = new DistancePredictionServiceTest();
//    test.shouldPredict();
class DistancePredictionServiceTest {
  @Test
  // should<что должен, если нужно, то когда>
  void shouldPredict() {
    // A(rrange)
    double consumption = 7.9;
    int volume = 20;
    // expected - ожидаемое
    int expected = 253;
    DistancePredictionService service = new DistancePredictionService();

    // A(ct)
    // actual - фактическое
    int actual = service.predict(consumption, volume);

    // A(ssert)
    assertEquals(expected, actual);
  }
}