## XML

Пролог:
``` xml
<?xml version="1.0" encoding="UTF-8"?>
```

Элементы (корневой только один, у него есть дети):
```xml
<tag attribute="value">
   content или другие теги 
</tag>
```

## Проектирование сервисов

1. Определять: 
   1. Только поведение - `Service` (method)
   2. Только характеристики и состояние - `Data` (`Model`)
   3. И то, и другое вместе - `Rich Model`
2. ...

Что-то, что предсказывает расстояние:
* volume
* consumption

## Git

`master`, `main`

## Package

1. `Main` - org.example
2. `*Service` - org.example.service

## Maven

Ctrl + Tab (зажав Ctrl) - удобное перемещение по вкладкам

Ctrl + Ctrl - запуск Maven-фаз

* `mvn clean` - очистка `target`
* `mvn package` - упаковка в JAR
* Ctrl + Shift + O - import изменений pom.xml в IDEA